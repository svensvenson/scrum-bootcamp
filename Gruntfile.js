module.exports = function(grunt) {

  // Project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      all: ['Gruntfile.js', 'js/*.js']
    },
    bower: {
      install: {
        options: {
          targetDir: './lib',
          layout: 'byComponent',
          install: true,
          verbose: false,
          cleanTargetDir: true,
          cleanBowerDir: false,
          bowerOptions: {}
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 9090,
          base: '.',
          keepalive: false,
          livereload: true
        }
      }
    },
    watch: {
      all:{
        options:{
          livereload: true
        },
        files: ['**/*.js', '**/*.html', '**/*.css']
      }
    },
    copy:{
      dist:{
        files: [
            {src: 'index.html', dest: 'dist/'},
            {src: 'images/*', dest: 'dist/'},
            {src: 'lib/**', dest: 'dist/'},
            {src: 'js/**', dest: 'dist/'},
            {src: 'css/**', dest: 'dist/'},
            {src: 'fonts/**', dest: 'dist/'},
            {src: 'templates/**', dest: 'dist/'},
            {src: 'data/**', dest: 'dist/'},            
        ]
      }
    },
    'ftp-deploy': {
      build: {
        auth: {
          host: 'svenadolph.net',
          port: 21,
          authKey: 'key1'
        },
        src: 'dist',
        dest: '/',
        exclusions: ['dist/.DS_Store', 'dist/**/Thumbs.db']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-bower-task'); //package manager
  grunt.loadNpmTasks('grunt-contrib-watch'); //used to trigger livereload of devserver when files change
  grunt.loadNpmTasks('grunt-contrib-connect'); //used to start server
  grunt.loadNpmTasks('grunt-contrib-copy'); //used to copy files in distribution folder
  grunt.loadNpmTasks('grunt-ftp-deploy'); //used to deploy dist folder to server via ftp

  grunt.registerTask('devServer', ['connect:server', 'watch:all']); //start dev server
  grunt.registerTask('dist', ['copy:dist']); //copy needed files to distribution folder
  grunt.registerTask('deployToServer', ['copy:dist', 'ftp-deploy']);
};