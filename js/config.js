require.config({
  shim: {

  },
  paths: {
    app: 'js/app',
    tmpl: 'templates',
  },
  "packages": [
          {
              "name": "backbone",
              "location": "lib/backbone/js/",
              "main": "backbone.js"
          },
          {
              "name": "handlebars",
              "location": "lib/handlebars/js/",
              "main": "handlebars.js"
          },
          {
              "name": "jquery",
              "location": "lib/jquery/js/",
              "main": "jquery.js"
          },
          {
              "name": "underscore",
              "location": "lib/underscore/js/",
              "main": "underscore.js"
          },
          {
              "name": "text",
              "location": "lib/requirejs-text/js/",
              "main": "text.js"
          }
      ]
});
