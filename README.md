# Scrum Bootcamp #

## Sinn & Zweck: ##
* Web-App, mit der man sich auf das Scrum-Zertifikat vorbereiten kann
* Fingerübung um backbone.js, Twitter Bootstrap, grunt etc. auszuprobieren

***

## Entwicklungsumgebung - Grunt ##
* Siehe "Grunt Getting Started Guide" - [http://gruntjs.com/getting-started](http://gruntjs.com/getting-started)
* In Kurz node, npm & grunt installieren & anschließend `npm install` ausführen.

### Development Server ###
Mit `grunt devServer` wird die Seite unter `http://localhost:9090` gehostet und automatisch neugeladen, sobald sich Dateien ändern.

### JS-Bibliotheken ###
Mit `grunt bower` werden alle verwendeten Bibliotheken geupdated.

### Deployment ###
Mit `grunt dist` werden alle zur Veröffentlichung benötigten Dateien in den Ordner `dist` kopiert.

Mit `grunt deployToServer` wird die Anwendung auf den FTP-Server deployed. Nutzername und Passwort sind in `.ftppass` hinterlegt. Die Datei ist nicht eingecheckt, einfach selbst die Datei im root Verzeichnis anlegen:

.ftppass

    {
      "key1": {
        "username": "???",
        "password": "???"
      }
    }

Achtung: Daten werden überschrieben, nicht gelöscht. D.h. nicht mehr verwendete Dateien werden z.B. nicht gelöscht.

***
    
## TODO: ##
* Farbenschema anpassen
* Zeitanzeige
* router implementieren (Browser Backbutton unterstützen & passende URLs)
* Auswertung abspeichern
* verschiedenen Fragen laden können
* grunt rumspielen z.B. uglify etc.
* ...

## DONE ##
* Besser dokumentieren
* Main app aufsplitten, einzelne View in Files
* Deploymenet und dist-Verzeichnis